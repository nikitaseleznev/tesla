module.exports = async function(options) {

    // Default options
    const {
        mongo = require('mongodb'),
        host = '192.168.91.5',
        port = 27017,
        dbName = 'chatApp'
    } = options

    // Connecting to db
    console.log("TESLA. Backend. Mongo. Connecting to db mongodb://"+host+":"+port+"/"+dbName)
    const db = await mongo.MongoClient.connect(`mongodb://${host}:${port}/${dbName}`)
    if (db) console.log("    Success!")

    let create = (collection, document) => db.collection(collection).insertOne(document)
    let read = (collection, query, options) => db.collection(collection).find(query, options).toArray()
    let update = (collection, query, document) => db.collection(collection).findOneAndUpdate(query, document, { returnOriginal: false })
    let _delete = (collection, query) => db.collection(collection).deleteOne(query)

    return {
        create,
        read,
        update,
        delete: _delete
    }
}
