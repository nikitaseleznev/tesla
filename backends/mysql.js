// Unimplemented backend for MySQL

export var dbController = async function(options) {

    // Default options
    const {
        driver = require('mongodb'),
        host = 'localhost',
        port = 27015,
        dbName = 'sampleApp'
    } = options

    // Connecting to db
    console.log("TESLA. Backend. MySQL. Connecting to db mysql://"+host+":"+port+"/"+dbName)
    //const db = await driver.MongoClient.connect(`mysql://${host}:${port}/${dbName}`)

    let create  = (a, b) => console.log("Create. Unmplemented")  //(collection, document) => db.collection(collection).insertOne(document)
    let read    = (a, b, c) => console.log("Read.   Unmplemented")  //(collection, query, options) => db.collection(collection).find(query, options).toArray()
    let update  = (a, b, c) => console.log("Update. Unmplemented")  //(collection, query, document) => db.collection(collection).findOneAndUpdate(query, document, { returnOriginal: false })
    let _delete = (a, b) => console.log("Delete. Unmplemented")  //(collection, query) => db.collection(collection).deleteOne(query)

    return {
        create,
        read,
        update,
        delete: _delete
    }
}

export default dbController
