// Unimplemented backend for Postgres

export var dbController = async function(options) {

    // Default options
    const {
        postgres = require('???'),
        host = 'localhost',
        port = 27015,
        dbName = 'sampleApp'
    } = options

    // Connecting to db
    console.log("TESLA. Backend. Postgres. Connecting to db postgres://"+host+":"+port+"/"+dbName)
    // const db = await ???.connect(`mongodb://${host}:${port}/${dbName}`)

    let create = (a=1, b=2, c=3, d=4) => console.log("Create. Unmplemented")  //(collection, document) => db.collection(collection).insertOne(document)
    let read = (a=1, b=2, c=3, d=4) => console.log("Read. Unmplemented")  ////(collection, query, options) => db.collection(collection).find(query, options).toArray()
    let update = (a=1, b=2, c=3, d=4) => console.log("Update. Unmplemented")  ////(collection, query, document) => db.collection(collection).findOneAndUpdate(query, document, { returnOriginal: false })
    let _delete = (a=1, b=2, c=3, d=4) => console.log("Delete. Unmplemented")  ////(collection, query) => db.collection(collection).deleteOne(query)

    return {
        create,
        read,
        update,
        delete: _delete
    }
}

export default dbController
