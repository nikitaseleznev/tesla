# TESLA.JS

Минималистичный фреймворк для написания приложений teslatele.com.
 > Хватит копипастить, пишем структурированный и логичный код!

### Версия
0.0.5a

## Tesla/core
 - Библиотечка router-client для работы с протоколом amqp


## Tesla/backends
Унифицированные бэк-энды к базам данных для создания примитивной CRUD-ORM
 - MongoDB
 - PostgreSQL (Unimliminted)
 - MySQL (Unimliminted)


## Tesla/models.js
Предоставляет удобный объект-обёртку для работы с сущностями. Например,
в пользовательском приложении можно определить
```
#!javascript
import { Model } from 'Tesla/models.js'

var Room = Model('Room', schema, options.db)
```
Где schema - json-schema сущности, а options.db - настройки базы данных,
в которой будет храниться данная модель.
(!) В одном приложении можно запросто работать с различными базами данных,
не задумываясь об этом.

Далее в пользовательских контроллерах, моделью можно пользоваться, например, так:
```
#!javascript
var Developer = Model('Developer', schema, options.db)

document = {name: "Nikita", age: 20}
let result = Developer.create(document)
console.log(Developer.collection)  // -> "Developer"
console.log(Developer.schema)      // -> schema_json сущности Developer
```


## Tesla/controllers.js
Объект Controller содержит в себе участки кода, которые вероятнее всего будут переписываться из приложения в приложение практически неизменно, оставляя пользователю право реализовать только логику конкретного приложения.

Если отсутствует реализация какого-либо метода, Controller заменяет его на stub (403 - Not Allowed)


## Пример использования.

Предлагаю ознакомиться с простым приложением tt_chat - чат с комнатами, личкой, аттачем,
(простенький клон HipChat) в качестве [примера использования][tt_chat] Tesla.

   [tt_chat]: <https://bitbucket.com/nikitaseleznev/tt_chat>