import mongo from './backends/mongo.js'
import postgres from "./backends/postgres.js"
import mysql from "./backends/mysql.js"

const backend = {
    mongo,
    postgres,  // Unimplemented
    mysql      // Unimplemented
}

var ObjectId = require('mongodb').ObjectId

async function Model(collection, schema, options) {

    console.log("TESLA. Model. Creating '" + collection + "'...")
	var db = await backend[options.backend](options)

    return {
        collection,
        schema,
        
        create: async function(document) {
            console.log("TESLA. Model. Creating new document for query '" + JSON.stringify(document) + "'")
            return db.create(collection, document)
        },

        read: async function(query, options) {
            console.log("TESLA. Model. Reading document " + JSON.stringify(query))
            return db.read(collection, query, options)
        },

        update: async function(where, query) {
            console.log("TESLA. Model. Updating document " + JSON.stringify(where))
            return db.update(collection, where, {$set: query})
        },

        delete: async function(query) {
        	console.log("TESLA. Model. Delete document " + JSON.stringify(query))
        	return db.delete(collection, query)
        }
    }
}

module.exports = { Model, ObjectId }
// export default Model

// user_models.js
//
// import { Model } from 'Tesla/models.js'
// var Room = Model('chat', schema, db_options)


// Usage of user_models.js
//
// import { M } from './models.js'
// doc = {a=a, b=b}
// let result = M.create(doc)