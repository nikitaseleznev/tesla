import _ from 'lodash'

var tv4 = require('tv4')
//!TODO generic
var ObjectId = require('mongodb').ObjectId

function checkQuery(model, content, required=false) {
    let query = _.filter(content, (val, key) => {
        return model.schema.properties.hasOwnProperty(key) && !model.schema.properties[key].readOnly
    })

    var result = tv4.validateMultiple(content, {
        type: 'object',
        additionalProperties: false,
        required: required && model.schema.required,
        properties: model.schema.properties
    })

    if (result.valid)
        return false
    return {
        code: 400,
        message: 'Bad Request',
        data: result.errors.map(item => ({
            message: item.message,
            dataPath: item.dataPath
        }))
    }
}

function prepareQuery(content) {
    for (var key in content) {
        if (Array.isArray(content[key])) {
            content[key].forEach(prepareQuery);
            content[key] = {
                $in: content[key]
            };
        } else if (key === 'createdAt' || key === 'updatedAt') {
            content[key] = {
                $gte: content[key].from,
                $lte: content[key].to
            };
        } else if (typeof content[key] === 'object') {
            prepareQuery(content[key]);
        } else if (key === '_id' || key.substr(-2) === 'Id') {
            content[key] = ObjectId(content[key]);
        }
    }
}

module.exports = function Controller({
	model,
	methods,
	models_list = ['Here should be listed all models of current application']}) {

	console.log("TESLA. Controller. Creating for " + model.collection + " model.")

	const {
		create = async function(request, model, document) {
			console.log("TESLA. Controller. Create stub 405 Not Allowed.")
	        return {code: 405, message: 'Method Not Allowed'}
	    },
		read = async function(request, model, query, options) {
			console.log("TESLA. Controller. Read stub 405 Not Allowed.")
	        return {code: 405, message: 'Method Not Allowed'}
	    },
		update = async function(request, model) {
			console.log("TESLA. Controller. Update stub 405 Not Allowed.")
	        return  {code: 405, message: 'Method Not Allowed'}
	    }
	} = methods

	// Fucking javascript delete
	if (!methods.hasOwnProperty("delete")) {
		methods.delete = async function(request, model, query) {
			console.log("TESLA. Controller. Delete stub 405 Not Allowed.")
	        return {code: 405, message: 'Not Allowed'}
	    }
	}

	return {
	    create: async function(request = {teamId, resource, fromAppId, headers, content}) {
	        console.log("TESLA. Controller. Create. ")
	        
	        let errorMessage = checkQuery(model, request.content, true)
	        if (errorMessage)
	            return errorMessage

		    var properties = model.schema.properties
		    var document = {}
		    for (var key in request.content) {
		        if (properties[key].readOnly)
		            continue
		        if (properties[key].$ref === 'ObjectId')
		            document[key] = ObjectId(request.content[key])
		        else
		            document[key] = request.content[key]
		    }

	        let result = await create(request, model, document)
	        console.log("TESLA. Controller. Create. Result: " + result)
	        return {code: 200, message: 'OK', data: { id: result.insertedId }}
	    },

	    read: async function(request = {teamId, resource, fromAppId, headers, content}) {
	        console.log("TESLA. Controller. Read. " + JSON.stringify(request.content))

	        let query = request.content.hasOwnProperty('query') ? request.content.query : {}
	        let options = request.content.hasOwnProperty('options') ? request.content.options : {}
	        if (query.id) {
	            query._id = query.id;
	            delete query.id;
	        }
	        prepareQuery(query)
	        let result

	       	try {
	        	result = await read(request, model, query, options)
		        console.log("TESLA. Controller. Read. Result: " + result)
		        return Promise.resolve({code: 200, message: 'OK', data: result})
	        } catch (e) {
	        	return {code: 400, message: e.message}
	        }
	    },

	    update: async function(request = {teamId, resource, fromAppId, headers, content}) {
	        console.log("TESLA. Controller. Update " + JSON.stringify(request.content))
	        
	        // let errorMessage = checkQuery(model, request.content, true)
	        // if (errorMessage)
	        //     return errorMessage

	        //try {
	        	let result = await update(request, model)
	        //} catch (e) {
	        //	return {code: 400, message: e.message}
	        //}
	        console.log("TESLA. Controller. Update. Result: " + JSON.stringify(result))
	        return {code: 200, message: 'OK'}
	    },

	    delete: async function(request = {teamId, resource, fromAppId, headers, content}) {
	        console.log("TESLA. Controller. Delete ")

	        let query = request.content.hasOwnProperty('query') ? request.content.query : {}
	        if (query.id) {
	            query._id = query.id;
	            delete query.id;
	        }
	        let result
	        try {
	       		result = await methods.delete(request, model, query)
	        } catch (e) {
	        	return {code: 400, message: e.message}
	        }
	        console.log("TESLA. Controller. Delete. Result: " + result)
	        return {code: 200, message: 'OK'}
	    },

	    // !TODO methods
	    options: async function({teamId, resource, fromAppId, headers, content}) {
	    	console.log("TESLA. Controller. Options ")
	    	let models_dict = {}
	    	for (var model_ of models_list)
	    		models_dict[model_.collection] = model_
	    	console.log(models_list, models_dict)
	        
	        if (!resource)
	            return {code: 200, message: 'OK', data: {
	            	schema: models_list.map((m) => { return m.schema; }),
	            	methods: models_list.map((m) => { return m.methods; }) 
	            }}

	        if (resource == '_')
	            return {code: 200, message: 'OK', data: { resources: models_dict }}

	        if (!models_dict.hasOwnProperty(resource))
	            return {code: 404, message: 'Not Found'}

	        return {code: 200, message: 'OK', data: { schema: models_dict[resource].schema, methods: models_dict[resource].methods }}
	    }
	}
}

